package ru.koptev.taskmanager.repository;

public interface InfoCommandRepo {

    int displayHelp();

    int displayExit();

    int displayAbout();

    int displayVersion();

    int displayError();

    void displayWelcome();

}
