package ru.koptev.taskmanager.repository;

public interface ManagementCommandRepo {

    int create();

    int clear();

    int list();

    void view(Object object);

    int viewByIndex();

    int viewById();

    int removeById();

    int removeByIndex();

    int removeByName();

    int updateByIndex();

    int updateById();

}
