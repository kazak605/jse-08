package ru.koptev.taskmanager;

import ru.koptev.taskmanager.repository.ManagementCommandRepo;
import ru.koptev.taskmanager.repository.InfoCommandRepo;
import ru.koptev.taskmanager.service.InfoService;
import ru.koptev.taskmanager.service.ProjectService;
import ru.koptev.taskmanager.service.TaskService;

import java.util.Scanner;

import static ru.koptev.taskmanager.constant.TerminalConstant.*;

public class Application {

    private static final Scanner scanner = new Scanner(System.in);

    private static final InfoCommandRepo infoService = new InfoService();

    private static final ManagementCommandRepo projects = new ProjectService();

    private static final ManagementCommandRepo tasks = new TaskService();

    public static void main(String[] args) {
        String inputString = "";
        infoService.displayWelcome();
        while (!EXIT.equals(inputString)) {
            inputString = scanner.nextLine();
            run(inputString);
        }
    }

    public static int run(String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case (HELP): return infoService.displayHelp();
            case (ABOUT): return infoService.displayAbout();
            case (VERSION): return infoService.displayVersion();
            case (EXIT): return infoService.displayExit();

            case (PROJECT_CREATE): return projects.create();
            case (PROJECT_CLEAR): return projects.clear();
            case (PROJECT_LIST): return projects.list();
            case (PROJECT_VIEW_BY_INDEX): return projects.viewByIndex();
            case (PROJECT_VIEW_BY_ID): return projects.viewById();
            case (PROJECT_REMOVE_BY_INDEX): return projects.removeByIndex();
            case (PROJECT_REMOVE_BY_ID): return projects.removeById();
            case (PROJECT_REMOVE_BY_NAME): return projects.removeByName();
            case (PROJECT_UPDATE_BY_INDEX): return projects.updateByIndex();
            case (PROJECT_UPDATE_BY_ID): return projects.updateById();

            case (TASK_CREATE): return tasks.create();
            case (TASK_CLEAR): return tasks.clear();
            case (TASK_LIST): return tasks.list();
            case (TASK_VIEW_BY_INDEX): return tasks.viewByIndex();
            case (TASK_VIEW_BY_ID): return tasks.viewById();
            case (TASK_REMOVE_BY_INDEX): return tasks.removeByIndex();
            case (TASK_REMOVE_BY_ID): return tasks.removeById();
            case (TASK_REMOVE_BY_NAME): return tasks.removeByName();
            case (TASK_UPDATE_BY_INDEX): return tasks.updateByIndex();
            case (TASK_UPDATE_BY_ID): return tasks.updateById();

            default: return infoService.displayError();
        }
    }

}
