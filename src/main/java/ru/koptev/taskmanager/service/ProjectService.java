package ru.koptev.taskmanager.service;

import ru.koptev.taskmanager.dao.ProjectDAO;
import ru.koptev.taskmanager.entity.Project;
import ru.koptev.taskmanager.repository.ManagementCommandRepo;

import java.util.Scanner;

public class ProjectService implements ManagementCommandRepo {

    private static final ProjectDAO projectDAO = new ProjectDAO();

    private static final Scanner scanner = new Scanner(System.in);

    @Override
    public int create() {
        System.out.println("Creating project");
        System.out.println("Please, enter project name...");
        projectDAO.create(scanner.nextLine());
        System.out.println("Ok");
        return 0;
    }

    @Override
    public int clear() {
        projectDAO.clear();
        System.out.println("Project clear!");
        return 0;
    }

    @Override
    public int list() {
        int index = 1;
        System.out.println("Project list:");
        System.out.format("%-10s%-20s%-20s%-20s%n", "Index", "Id", "Name", "Description");
        for (Project project : projectDAO.findAll()) {
            System.out.printf("%-10d%-20d%-20s%-20s%n", index, project.getId(), project.getName(), project.getDescription());
            index++;
        }
        return 0;
    }

    @Override
    public void view(Object object) {
        if (object == null) return;
        Project project = (Project) object;
        System.out.println("View project:");
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
    }

    @Override
    public int viewByIndex() {
        System.out.println("Please, enter project index...");
        int index = Integer.parseInt(scanner.nextLine()) - 1;
        if (index < 0 || index > projectDAO.findAll().size()) {
            System.out.println("Wrong index!");
            return -1;
        }
        view(projectDAO.findByIndex(index));
        return 0;
    }

    @Override
    public int viewById() {
        System.out.println("Please, enter project id...");
        long id = Long.parseLong(scanner.nextLine());
        Project project = projectDAO.findById(id);
        if (project != null) {
            view(project);
            return 0;
        }
        System.out.println("Project not found!");
        return -1;
    }

    @Override
    public int removeById() {
        System.out.println("Deleting a project by id");
        System.out.println("Please, enter project id...");
        long id = Long.parseLong(scanner.nextLine());
        boolean deleteStatus = projectDAO.removeById(id);
        if (deleteStatus) {
            System.out.println("Project successfully deleted by id");
            return 0;
        }
        System.out.println("Project not found!");
        return -1;
    }

    @Override
    public int removeByIndex() {
        System.out.println("Deleting a project by index");
        System.out.println("Please, enter project index...");
        int index = Integer.parseInt(scanner.nextLine()) - 1;
        if (index < 0 || index > projectDAO.findAll().size()) {
            System.out.println("Wrong index!");
            return -1;
        }
        projectDAO.removeByIndex(index);
        System.out.println("Project successfully deleted by index");
        return 0;
    }

    @Override
    public int removeByName() {
        System.out.println("Deleting a project by name");
        System.out.println("Please, enter project name...");
        String name = scanner.nextLine();
        boolean deleteStatus = projectDAO.removeByName(name);
        if (deleteStatus) {
            System.out.println("Project successfully deleted by name");
            return 0;
        }
        System.out.println("Project not found!");
        return -1;
    }

    @Override
    public int updateByIndex() {
        System.out.println("Updating a project by index");
        System.out.println("Please, enter project index...");
        int index = Integer.parseInt(scanner.nextLine()) - 1;
        if (index < 0 || index > projectDAO.findAll().size()) {
            System.out.println("Wrong index!");
            return -1;
        }
        System.out.println("Please, enter new project name...");
        String name = scanner.nextLine();
        System.out.println("Please, enter new project description...");
        String description = scanner.nextLine();
        projectDAO.updateByIndex(index, name, description);
        System.out.println("Project successfully updated by index");
        return 0;
    }

    @Override
    public int updateById() {
        System.out.println("Updating a project by id");
        System.out.println("Please, enter project id...");
        long id = Long.parseLong(scanner.nextLine());
        System.out.println("Please, enter new project name...");
        String name = scanner.nextLine();
        System.out.println("Please, enter new project description...");
        String description = scanner.nextLine();
        boolean updateStatus = projectDAO.updateById(id, name, description);
        if (updateStatus) {
            System.out.println("Project successfully deleted by id");
            return 0;
        }
        System.out.println("Project not found!");
        return -1;
    }

}
