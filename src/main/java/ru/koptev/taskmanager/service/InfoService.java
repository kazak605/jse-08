package ru.koptev.taskmanager.service;

import ru.koptev.taskmanager.repository.InfoCommandRepo;

public class InfoService implements InfoCommandRepo {

    public int displayHelp() {
        System.out.println("System commands:");
        System.out.println("about - Display developer information.");
        System.out.println("version - Display program version.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println();
        System.out.println("Project commands:");
        System.out.println("project-create - Create new project.");
        System.out.println("project-clear - Clear all projects.");
        System.out.println("project-list - Display all projects.");
        System.out.println("project-view-by-index - Display project by index.");
        System.out.println("project-view-by-id - Display project by id.");
        System.out.println("project-remove-by-index - Delete project by index.");
        System.out.println("project-remove-by-id - Delete project by id.");
        System.out.println("project-remove-by-name - Delete project by name");
        System.out.println("project-update-by-index - Update project by index.");
        System.out.println("project-update-by-id - Update project by id");
        System.out.println();
        System.out.println("Task commands:");
        System.out.println("task-create - Create new task.");
        System.out.println("task-clear - Clear all tasks.");
        System.out.println("task-list - Display all tasks.");
        System.out.println("task-view-by-index - Display task by index.");
        System.out.println("task-view-by-id - Display task by id.");
        System.out.println("task-remove-by-index - Delete task by index.");
        System.out.println("task-remove-by-id - Delete task by id.");
        System.out.println("task-remove-by-name - Delete task by name.");
        System.out.println("task-update-by-index - Update task by index.");
        System.out.println("task-update-by-id - Update task by id.");
        return 0;
    }

    public int displayExit() {
        System.out.println("Terminate program...");

        return -1;
    }

    public int displayAbout() {
        System.out.println("Name: Koptev Anton");
        System.out.println("Email: koptev_av@nlmk.com");
        return 0;
    }

    public int displayVersion() {
        System.out.println("Version: 1.0.8");
        return 0;
    }

    public int displayError() {
        System.out.println("Error! Invalid parameter...");
        return 0;
    }

    public void displayWelcome() {
        System.out.println("Welcome to Task Manager");
    }

}
