package ru.koptev.taskmanager.service;

import ru.koptev.taskmanager.dao.TaskDAO;
import ru.koptev.taskmanager.entity.Task;
import ru.koptev.taskmanager.repository.ManagementCommandRepo;

import java.util.Scanner;

public class TaskService implements ManagementCommandRepo {
    private static final TaskDAO taskDAO = new TaskDAO();

    private static final Scanner scanner = new Scanner(System.in);

    @Override
    public int create() {
        System.out.println("Creating task");
        System.out.println("Please, enter project name...");
        taskDAO.create(scanner.nextLine());
        System.out.println("Ok");
        return 0;
    }

    @Override
    public int clear() {
        taskDAO.clear();
        System.out.println("Task clear!");
        return 0;
    }

    @Override
    public int list() {
        int index = 1;
        System.out.println("Task list:");
        System.out.format("%-10s%-20s%-20s%-20s%n", "Index", "Id", "Name", "Description");
        for (Task task : taskDAO.findAll()) {
            System.out.printf("%-10d%-20d%-20s%-20s%n", index, task.getId(), task.getName(), task.getDescription());
            index++;
        }
        return 0;
    }

    @Override
    public void view(Object object) {
        if (object == null) return;
        Task task = (Task) object;
        System.out.println("View task:");
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
    }

    @Override
    public int viewByIndex() {
        System.out.println("Please, enter task index...");
        int index = Integer.parseInt(scanner.nextLine()) - 1;
        if (index < 0 || index > taskDAO.findAll().size()) {
            System.out.println("Wrong index!");
            return -1;
        }
        view(taskDAO.findByIndex(index));
        return 0;
    }

    @Override
    public int viewById() {
        System.out.println("Please, enter task id...");
        long id = Long.parseLong(scanner.nextLine());
        Task task = taskDAO.findById(id);
        if (task != null) {
            view(task);
            return 0;
        }
        System.out.println("Project not found!");
        return -1;
    }

    @Override
    public int removeById() {
        System.out.println("Deleting a task by id");
        System.out.println("Please, enter task id...");
        long id = Long.parseLong(scanner.nextLine());
        boolean deleteStatus = taskDAO.removeById(id);
        if (deleteStatus) {
            System.out.println("Task successfully deleted by id");
            return 0;
        }
        System.out.println("Task not found!");
        return -1;
    }

    @Override
    public int removeByIndex() {
        System.out.println("Deleting a task by index");
        System.out.println("Please, enter task index...");
        int index = Integer.parseInt(scanner.nextLine()) - 1;
        if (index < 0 || index > taskDAO.findAll().size()) {
            System.out.println("Wrong index!");
            return -1;
        }
        taskDAO.removeByIndex(index);
        System.out.println("Task successfully deleted by index");
        return 0;
    }

    @Override
    public int removeByName() {
        System.out.println("Deleting a task by name");
        System.out.println("Please, enter task name...");
        String name = scanner.nextLine();
        boolean deleteStatus = taskDAO.removeByName(name);
        if (deleteStatus) {
            System.out.println("Task successfully deleted by name");
            return 0;
        }
        System.out.println("Task not found!");
        return -1;
    }

    @Override
    public int updateByIndex() {
        System.out.println("Updating a task by index");
        System.out.println("Please, enter task index...");
        int index = Integer.parseInt(scanner.nextLine()) - 1;
        if (index < 0 || index > taskDAO.findAll().size()) {
            System.out.println("Wrong index!");
            return -1;
        }
        System.out.println("Please, enter new task name...");
        String name = scanner.nextLine();
        System.out.println("Please, enter new task description...");
        String description = scanner.nextLine();
        taskDAO.updateByIndex(index, name, description);
        System.out.println("Task successfully updated by index");
        return 0;
    }

    @Override
    public int updateById() {
        System.out.println("Updating a task by id");
        System.out.println("Please, enter task id...");
        long id = Long.parseLong(scanner.nextLine());
        System.out.println("Please, enter new task name...");
        String name = scanner.nextLine();
        System.out.println("Please, enter new task description...");
        String description = scanner.nextLine();
        boolean updateStatus = taskDAO.updateById(id, name, description);
        if (updateStatus) {
            System.out.println("Task successfully deleted by id");
            return 0;
        }
        System.out.println("Task not found!");
        return -1;
    }

}
