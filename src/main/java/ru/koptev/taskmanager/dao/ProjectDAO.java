package ru.koptev.taskmanager.dao;

import ru.koptev.taskmanager.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectDAO {

    private List<Project> projects = new ArrayList<>();

    public Project create(final String name) {
        Project project = new Project(name);
        projects.add(project);
        return project;
    }

    public Project create(final String name, final String description) {
        Project project = new Project(name);
        project.setDescription(description);
        projects.add(project);
        return project;
    }

    public void clear() {
        projects.clear();
    }

    public List<Project> findAll() {
        return projects;
    }

    public Project findByIndex(int index) {
        return projects.get(index);
    }

    public Project findById(Long id) {
        for (Project project : projects) {
            if (project.getId() == id) {
                return project;
            }
        }
        return null;
    }

    public void removeByIndex(int index) {
        projects.remove(index);
    }

    public boolean removeById(Long id) {
        for (Project project : projects) {
            if (project.getId() == id) {
                projects.remove(project);
                return true;
            }
        }
        return false;
    }

    public boolean removeByName(String name) {
        for (Project project : projects) {
            if (project.getName().equals(name)) {
                projects.remove(project);
                return true;
            }
        }
        return false;
    }

    public void updateByIndex(int index, String name, String description) {
        Project project = projects.get(index);
        project.setName(name);
        project.setDescription(description);
    }

    public boolean updateById(Long id, String name, String description) {
        for (Project project : projects) {
            if (project.getId() == id) {
                project.setName(name);
                project.setDescription(description);
                return true;
            }
        }
        return false;
    }

}
