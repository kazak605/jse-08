package ru.koptev.taskmanager.dao;

import ru.koptev.taskmanager.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskDAO {

    private List<Task> tasks = new ArrayList<>();

    public Task create(final String name) {
        Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    public Task create(final String name, final String description) {
        Task task = new Task(name);
        task.setDescription(description);
        tasks.add(task);
        return task;
    }

    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() {
        return tasks;
    }

    public Task findByIndex(int index) {
        return tasks.get(index);
    }

    public Task findById(Long id) {
        for (Task task : tasks) {
            if (task.getId() == id) {
                return task;
            }
        }
        return null;
    }

    public void removeByIndex(int index) {
        tasks.remove(index);
    }

    public boolean removeById(Long id) {
        for (Task task : tasks) {
            if (task.getId() == id) {
                tasks.remove(task);
                return true;
            }
        }
        return false;
    }

    public boolean removeByName(String name) {
        for (Task task : tasks) {
            if (task.getName().equals(name)) {
                tasks.remove(task);
                return true;
            }
        }
        return false;
    }

    public void updateByIndex(int index, String name, String description) {
        Task task = tasks.get(index);
        task.setName(name);
        task.setDescription(description);
    }

    public boolean updateById(Long id, String name, String description) {
        for (Task task : tasks) {
            if (task.getId() == id) {
                task.setName(name);
                task.setDescription(description);
                return true;
            }
        }
        return false;
    }

}
